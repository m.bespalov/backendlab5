create table usersuperpower
(
    id    int (10) unsigned not null,
    power int (10) unsigned not null,
    primary key (id,power)
);
create table userbase
(
    id    int (10) unsigned not null auto_increment,
    name  varchar(128) not null default '',
    year  int (10) not null default 0,
    sex   int(5) not null default 0,
    email varchar(128) not null default '',
    bio   varchar(128)          default '',
    limb  int(5) not null default 0,
    primary key (id)
);
create table userpassword
(
    id    int (10) unsigned not null,
    login varchar(128) not null default '',
    pwd   varchar(128) not null default '',
    primary key (id)
);

select * from userbase;
select * from usersuperpower;
select * from userpassword;

drop table userbase;
drop table userpassword;
drop table usersuperpower;

ssh u40075@212.192.134.20
cd ../../var/www/html/u40075

user6094fcca536b0   pass6094fcca536b6
user6094fd3f6007b   pass6094fd3f6007f
user6094fec38da44   pass6094fec38da4b